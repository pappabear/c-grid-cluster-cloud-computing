package ro.bbs.bogdan;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class InvertedIndex
{
    private static List<Text> stopwords = new ArrayList<Text>();

    public static class TokenizerMapper extends Mapper<Object, Text, Text, Text>
    {
        // the current word holder
        private Text word = new Text();
        private Text filename = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            // we get the name of the file
            FileSplit fileSplit = (FileSplit)context.getInputSplit();

            // the iterator
            StringTokenizer itr = new StringTokenizer(value.toString().toLowerCase());
            // while we have more words
            while (itr.hasMoreTokens())
            {
                // we get the current word
                word.set(itr.nextToken());
                // we get the current file
                filename.set(fileSplit.getPath().getName());
                // if it's not a stopword
                if(!stopwords.contains(word))
                {
                    // we write the word and the belonging file
                    context.write(word, filename);
                }
            }
        }
    }

    public static class FilenameReducer extends Reducer<Text,Text,Text,Text>
    {
        private Text result = new Text();

//        public void reduce(Text key, Iterator<Text> values,
//                           OutputCollector<Text, Text> output, Reporter reporter)
//                throws IOException {
//
//            // Text's equals() method should be overloaded to make this work
//            Set<Text> outputValues = new HashSet<Text>();
//
//            while (values.hasNext()) {
//                // make a new Object because Hadoop may mess with original
//                Text value = new Text(values.next());
//
//                // takes care of removing duplicates
//                outputValues.add(value);
//            }
//
//            boolean first = true;
//            StringBuilder toReturn = new StringBuilder();
//            Iterator<Text> outputIter = outputValues.iterator();
//            while (outputIter.hasNext()) {
//                if (!first) {
//                    toReturn.append(", ");
//                }
//                first = false;
//                toReturn.append(outputIter.next().toString());
//            }
//
//            output.collect(key, new Text(toReturn.toString()));
//        }

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            // we remove the duplicates
            Set<Text> filenamesSet = new HashSet<Text>();

            for (Text filename : values)
            {
                filenamesSet.add(filename);
            }

            StringBuilder filenamesListString = new StringBuilder();
            filenamesListString.append("|");

            for (Text filename : filenamesSet)
            {
                filenamesListString.append(" ").append(filename.toString()).append( " |");
            }

            // we set the list string as a result
            result.set(filenamesListString.toString());
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "inverted index");
        job.setJarByClass(InvertedIndex.class);
        job.setMapperClass(TokenizerMapper.class);
        //job.setCombinerClass(FilenameReducer.class);
        job.setReducerClass(FilenameReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        String stopwordsPath = args.length >=2 && args[2] != null ? args[2] : "/user/root/invertedindex/stopwords";
        try{
            Path pt=new Path(stopwordsPath);//Location of file in HDFS
            FileSystem fs = FileSystem.get(conf);
            BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(pt)));
            String line;
            line=br.readLine();
            Text currentStopword = new Text();
            while (line != null){
                currentStopword.set(line.trim());
                stopwords.add(currentStopword);
                line=br.readLine();
            }
        }
        catch(Exception e)
        {
            System.out.println("Invalid stopwords file :( ");
        }
        String inputPath = args.length >=1 && args[0] != null ? args[0] : "/user/root/invertedindex/inputbooks";
        String outputPath = args.length >=2 && args[1] != null ? args[1] : "/user/root/invertedindex/output";
        FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));


        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
