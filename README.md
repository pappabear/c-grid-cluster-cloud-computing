# c-grid-cluster-cloud-computing

Course - Grid, Cluster and Cloud Computing for university in Hadoop and Azure


"With great power comes great responsibility, the styling is not one of them..."

 _    _ _           _     _       _               _                  ___  
| |  | | |         | |   (_)     | |             | |                |__ \ 
| |  | | |__   __ _| |_   _ ___  | |__   __ _  __| | ___   ___  _ __   ) |
| |/\| | '_ \ / _` | __| | / __| | '_ \ / _` |/ _` |/ _ \ / _ \| '_ \ / / 
\  /\  / | | | (_| | |_  | \__ \ | | | | (_| | (_| | (_) | (_) | |_) |_|  
 \/  \/|_| |_|\__,_|\__| |_|___/ |_| |_|\__,_|\__,_|\___/ \___/| .__/(_)  
                                                               | |        
                                                               |_|       


======= Hadoop ecosystem =======================================

-------------------------------------------------
HDFS - hadoop distributed file system 
-------------------------------------------------
make all harddrives look like a gigant single file system
Handle big files
Breaks them into blocks (128MB blocks)
Stores the blocks across computers (more copies for backup purposes)
HDFS manages the storage
YARN manages the processing of the storage

Usage:
> UI(Ambari)
> CLI
> HTTP
> Java
> NFS Gateway (Linux mount File System to server)


Components under the hood:
> Name node
	- Table of file names and reference to the blocks of each file
	- Where everything is, what is moved, what is created
	- Keeps track of what's on the data nodes
> Data nodes
	- Contian the blocks 

Scenario:
> Reading a file 
Client -> Name node: I want myFile
Name node -> Client: myFile is on these blocks from these data nodes
Client takes the mentioned blocks from the mentioned data nodes



> Writing a file
Client -> Name node: I want to create newFile 
Name node -> Client: Ok I created an Entry
Client -> Data node: Store my newFile
Data node -> Data nodes: Manage the storing
Data node -> Client: Ok, here is your file
Client -> Name node: Ok, here is my file

> Name node fails (Bursts into falmes and stuff)
Good idea to do constant backup of metadata



-------------------------------------------------
YARN - yet another resource negotiator
-------------------------------------------------
processing part of hadoop, what gets to run tasks when, manage nodes, 
the hartbeat of the cluster


-------------------------------------------------
MapReduce
-------------------------------------------------
programming model that allows to process data across an entire cluster
mappers - transform data in paralel across cluster efficiently
reducers - agregate data together

Written in Java
Through Streaming it alows comunication with other languages using
stdin	stdout for processes

What it does:
We have our movie data
user_id 	movie_id	rating

We visualise them as an directed graph (from user_id to movie_id)
We MAP for each user_id all the adjacent movie_ids 
We SORT them by user_id
(user_id_1: movie_id_2, movie_id_100)	
(user_id_7: movie_id_10, movie_id_50, movie_id_3, movie_id_40)  
...

We REDUCE them
(user_id_1: 2)
(user_id_7: 4)
...


How it does it:
Client node calls the MapReducer
Talks to Yarn to get input about the state of the machines, what gets run where
Copy data that might need to be processed to the HDFS to be
accessible for the other processing nodes
NodeManager (NodeReduce Application Master) has NodeManager nodes under it
	Comunicates with Yarn to know the state of the children nodes
	The children nodes perform MapTasks/ReduceTask over the mentioned data from HDFS



-------------------------------------------------
Pig 
-------------------------------------------------
Built over MapReduce, more high level API
Scripting for interogations
Write scripts like Queries that are mapped to MapReduce commands



Hive
------
Built over MapReduce, more high level API
Scripting for interogations by treating the HDFS like a relational database
Write scripts like Queries more like SQL



Ambari 
-------
Birdseye view upon system
Manage the system resources
Execute queries (like Hive queries) from views
Import databases



Mesos
-------
Alternative to Yarn 

 

Spark
------
Same level as MapReduce
Process data
Write spark scripts (python, java)
Machine learning 
Streaming data
Queries


Tez
-----
Same level as MapReduce
Acyclic Graph
Works together with HIVE (Hive through Tez instead of Hive through MapReduce)


HBase
------
NoSQL database
Expose data as NoSql to other systems


Storm
------
Process streaming data (from sensors for example)
Ex transform incoming data into a database


Oozie
------
Schedule jobs reguarding the ecosystem


Zookeepr
---------
Keep track of the ecosystem (nodes, logs, etc)


Data ingestors:
================
Sqoop
------
How to get data into ecosystem from external sources
Tie hadoop database into relational database
Make it accessible to JDBC
(Or make JDBC act like part of the filesystem not sure)


Flume
------
Listen to weblogs from multiple servers in realtime and publis them into the ecosystem


Kafka
------
Colest data from a cluster of PCs, Webservers and integrate them into your cluster


Start hadoop ecosystem
-----------------------
> Download hadoop sandbox for WMware from 
https://hortonworks.com/downloads/

> import into WMware
> start the virtual machine



External Storage:
===================
MySQL
------
Spark can write to any SQL database

Cassandra
----------
Expose data for real time usage (web applications)

MongoDB
--------
Expose data for real time usage (web applications)




Query engines (like Hive):
===================
Drill
------
Write SQL queries to work across NoSql databases

Phoenix
--------
Similar to Drill but extends the relational "wrapper" to non relational data

Hue
----
Create queries for Hive and Hbase
Can even take the role of Ambari (Cloudera)




Basic data
-------------

Download the 100k old movies dataset archive from
https://grouplens.org/datasets/movielens/


u.data -> user id, movie id, rating
u.item -> metadata 





Hadoop overview with ambari
----------------------------

> Browser on 
http://192.168.181.128:1080 (for WM Ware)

> Launch Dashboard

> Login
user: maria_dev
pass: maria_dev



Query with HIVE
-----------------

> HIVE view (upper right square)
> Upload table
	> File type: CSV - Configure delimiter - Choose file
		> u.data
			> Upload table
		> u,.items
			> Upload table
> Query
	> new items will be in the "default" database
	> SELECT * FROM movie_names;
	> Visualisation options (chart icon)




SSH Connect
---------------
(With Putty on windows)
(With ssh on Ubuntu)
Hostname: maria_dev@127.0.0.1
Port:2222
*SSH
Password: maria_dev

> same username on the foreign machine
ssh [remote_host]

> diffrent username on the foreign machine
ssh [remote_username@remote_host]





Download files from the web
-----------------------------
wget http://media.sundog-soft.com/hadoop/ml-100k/u.data
wget http://media.sundog-soft.com/hadoop/RatingsBreakdown.py





CLI Upload files to HDFS
--------------------------

>SSH Connect

> List files
hadoop fs -ls [directory_path]

> Make directory
hadoop fs -mkdir newDirName 

> Upload file from local machine
hadoop fs -copyFromLocal u.data ml-100k/u.data

> Remove directory
hadoop fs -rm [directory_name]   

> See the rest of commands
hadoop fs





Install packages (python, nano)
--------------------------------

> SSH Connect

> Enter root
su root
default password: hadoop
 
 > install pip for python
 yum install python-pip
 
 > install nano
 yum install nano

 > install map reduce job (mrjob)
 pip install mrjob





Run Map Reduce Job (mrjob)
---------------------------

python [job_name.py] -r hadoop --hadoop-streaming-jar /usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar ([data_file] to copy the file into hdfs | [hdfs://data_file] if the file is already into the hdfs)



































   _____      _                 _    _           _                   
  / ____|    | |               | |  | |         | |                  
 | (___   ___| |_ _   _ _ __   | |__| | __ _  __| | ___   ___  _ __  
  \___ \ / _ \ __| | | | '_ \  |  __  |/ _` |/ _` |/ _ \ / _ \| '_ \ 
  ____) |  __/ |_| |_| | |_) | | |  | | (_| | (_| | (_) | (_) | |_) |
 |_____/ \___|\__|\__,_| .__/  |_|  |_|\__,_|\__,_|\___/ \___/| .__/ 
                       | |                                    | |    
                       |_|                                    |_|    

========================================
How to Hadoop on a multi-node cluster
========================================

* Setup a 3 node cluster (A physical grouping of servers)
* As long as we have the same OS on all the nodes, we can go up to unlimited nodes easy :)


-------------------------------------------
Hadoop Architecture
-------------------------------------------
Storage 	---> HDFS ---> Name Node + Data Node
Processing	---> YARN ---> Resource Manager + Node Manager



-------------------------------------------
Steps Overview: 
-------------------------------------------
> Download and install Java and Hadoop on all the systems (Masters and slaves)

> Specify the IP address of each system followed by their host names in hosts file inside /etc/ folder of each system

> Configure hadoop configuration files (core-site, hdfs-site, mapred-site, yarn-site)

> Edit slaves file on master node

> Format namenode and start all hadoop services

> Check live nodes on Hadoop namenode UI




-------------------------------------------
> On Windows switch from hadoop to docker credentials 
-------------------------------------------
bcdedit /set hypervisorlaunchtype off
bcdedit /set hypervisorlaunchtype auto




-------------------------------------------
> Install VMWare Player 
-------------------------------------------
sudo apt update && sudo apt install build-essential && sudo apt install linux-headers-$(uname -r)
wget https://www.vmware.com/go/getplayer-linux
chmod +x getplayer-linux
sudo ./getplayer-linux



-------------------------------------------
> Download CentOS Minimal ISO
-------------------------------------------
https://www.centos.org/download/



-------------------------------------------
> Create 3 CentOS VMs in VMWare
-------------------------------------------



-------------------------------------------
> See ip address of each machine (from within each)
-------------------------------------------
ip addr


nn1–192.168.43.73 HDFS — Namenode and YARN — Resourcemanager
dn1–192.168.43.9 HDFS — Datanode and YARN — Nodemanager
dn2–192.168.43.137 HDFS — Datanode and YARN — Nodemanager



-------------------------------------------
> Connect with ssh to each machine
-------------------------------------------
ssh root@192.168.43.73
ssh root@192.168.43.9
ssh root@192.168.43.137



-------------------------------------------
> edit /etc/hosts
-------------------------------------------
vi /etc/hosts
file must look like:
-------------------------------------------
#127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localhostdomain4
#::1         localhost localhost.localdomain localhost6 localhost6.localhostdomain6
192.168.43.73	nn1	nn1.cluster1.com
192.168.43.9	dn1	dn1.cluster1.com
192.168.43.137	dn2	dn2.cluster1.com
--------------------------------------------
press "i" to enter --INSERT-- mode
paste with "CTRL" + "SHIFT" + "V"
press "ESC" to stop --INSERT-- mode
press "SHIFT" + ":" to enter some sort of option mode
press "w" then "q" then "!" then "ENTER"





-------------------------------------------
> Install wget on each of them
-------------------------------------------
yum install wget



-------------------------------------------
> Install java 7 on each of them
-------------------------------------------
sudo yum install java-1.7.0-openjdk-devel
java home : cd usr/lib/jvm/java-1.7.0-openjdk-1.7.0.211-2.6.17.1.el7_6.x86_64/jre




-------------------------------------------
> Install hadoop 2.7.3
-------------------------------------------
wget https://archive.apache.org/dist/hadoop/core/hadoop-2.7.3/hadoop-2.7.3.tar.gz
mkdir /home/hadoop
tar -xzvf /root/hadoop-2.7.3.tar.gz -C /home/hadoop/





-------------------------------------------
> Add environment variables to ~/.bash_profile
--------------------------------------------
export JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.211-2.6.17.1.el7_6.x86_64/jre
export PATH=$PATH:$JAVA_HOME/bin
export CLASSPATH=.:$JAVA_HOME/jre/lib:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar
# Set Hadoop-related environment variables
export HADOOP_HOME=/home/hadoop/hadoop-2.7.3
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
# Add Hadoop bin/ directory to PATH
export PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin
--------------------------------------------
source it with
. ~/.bash_profile




-------------------------------------------
> Add java variable to hadoop-env.sh
--------------------------------------------
cd ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/
vi hadoop-env.sh
--------------------------------------------
#The java implementation to use.
export JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.211-2.6.17.1.el7_6.x86_64/jre
--------------------------------------------





-------------------------------------------
> Modify core-site.xml
-------------------------------------------
ADD TO core-site.xml
(And be careful to configure the IP with your nn IP)
vi ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/core-site.xml
--------------------------------------------
<configuration>
<property>
	<name>fs.default.name</name>
	<value>hdfs://192.168.43.73:9000</value>
</property>
</configuration>
--------------------------------------------




-------------------------------------------
> Modify hdfs-site.xml
-------------------------------------------
ADD TO hdfs-site.xml
vi ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/hdfs-site.xml
--------------------------------------------
<configuration>
<property>
<name>dfs.namenode.name.dir</name>
<value>file:/home/hadoop/hadoop-2.7.3/hadoop_store/hdfs/namenode2</value>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>file:/home/hadoop/hadoop-2.7.3/hadoop_store/hdfs/datanode2
</value>
</property>
</configuration>
--------------------------------------------




-------------------------------------------
> Modify mapred-site.xml
-------------------------------------------
ADD TO mapred-site.xml
cd ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/
cp mapred-site.xml.template mapred-site.xml
vi mapred-site.xml
--------------------------------------------
<configuration>
<property>
<name>mapreduce.framework.name</name>
<value>yarn</value>
</property>
</configuration>
--------------------------------------------





-------------------------------------------
> Modify yarn-site.xml
-------------------------------------------
ADD TO yarn-site.xml
vi ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/yarn-site.xml
--------------------------------------------
<configuration>

<property>
	<name>yarn.nodemanager.aux-services</name>
	<value>mapreduce_shuffle</value>
</property>

<property>
	<name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
	<value>org.apache.hadoop.mapred.ShuffleHandler</value>
</property>

<property>
	<name>yarn.resourcemanager.resource-tracker.address</name>
	<value>192.168.43.73:8025</value>
</property>

<property>
	<name>yarn.resourcemanager.scheduler.address</name>
	<value>192.168.43.73:8030</value>
</property>

<property>
	<name>yarn.resourcemanager.address</name>
	<value>192.168.43.73:8050</value>
</property>
<configuration>
--------------------------------------------




-------------------------------------------
> Create Namenode directory in nn1
-------------------------------------------
mkdir -p ~/../../home/hadoop/hadoop-2.7.3/hadoop_store/hdfs/namenode2




-------------------------------------------
> Modify slaves files and add both datanodes ip’s
-------------------------------------------
replace the whole content of
vi ~/../../home/hadoop/hadoop-2.7.3/etc/hadoop/slaves
-------------------------------------------
192.168.43.9
192.168.43.137
-------------------------------------------




-------------------------------------------
(Optional approach if you havent created the files individually)
> Copy all the modified files to dn1 and dn2
-------------------------------------------
scp [all file names go here] dn:/../../home/hadoop/hadoop-2.7.3/etc/hadoop/




-------------------------------------------
> Make datanode directory with premissions on both dn1 and dn2
-------------------------------------------
mkdir -p ~/../../home/hadoop/hadoop-2.7.3/hadoop_store/hdfs/datanode2/
chmod 755 ~/../../home/hadoop/hadoop-2.7.3/hadoop_store/hdfs/datanode2/





-------------------------------------------
(May generate error but it's ok)
> Disable the firewall
-------------------------------------------
service iptables stop
service ip6tables stop

systemctl status firewalld && systemctl stop firewalld && systemctl disable firewalld



-------------------------------------------
> Start Namenode, Datanode (start-dfs) and Resource Manager, Nodemanager (start-yarn)(on all)
-------------------------------------------
~/../../home/hadoop/hadoop-2.7.3/sbin/start-dfs.sh
~/../../home/hadoop/hadoop-2.7.3/sbin/start-yarn.sh



-------------------------------------------
> Jps on the nodes
-------------------------------------------
cd ~/../../home/hadoop/hadoop-2.7.3/ && jps



-------------------------------------------
> Health report to check startup
-------------------------------------------
hdfs dfsadmin -report



-------------------------------------------
> Overview from browser
-------------------------------------------
http://192.168.43.73:50070/dfshealth.html#tab-overview



-------------------------------------------
> Resource Manager from browser
-------------------------------------------
http://192.168.43.73:8088/cluster



-------------------------------------------
> How to stop hadoop
-------------------------------------------
~/../../home/hadoop/hadoop-2.7.3/sbin/stop-dfs.sh
~/../../home/hadoop/hadoop-2.7.3/sbin/stop-yarn.sh

































______ _                        _ _   _       _               _                   
| ___ \ |                      (_) | | |     | |             | |                  
| |_/ / | __ _ _   _  __      ___| |_| |__   | |__   __ _  __| | ___   ___  _ __  
|  __/| |/ _` | | | | \ \ /\ / / | __| '_ \  | '_ \ / _` |/ _` |/ _ \ / _ \| '_ \ 
| |   | | (_| | |_| |  \ V  V /| | |_| | | | | | | | (_| | (_| | (_) | (_) | |_) |
\_|   |_|\__,_|\__, |   \_/\_/ |_|\__|_| |_| |_| |_|\__,_|\__,_|\___/ \___/| .__/ 
                __/ |                                                      | |    
               |___/                                                       |_|    


=======================================================================

 _    _               _   _____                   _   
| |  | |             | | /  __ \                 | |  
| |  | | ___  _ __ __| | | /  \/ ___  _   _ _ __ | |_ 
| |/\| |/ _ \| '__/ _` | | |    / _ \| | | | '_ \| __|
\  /\  / (_) | | | (_| | | \__/\ (_) | |_| | | | | |_ 
 \/  \/ \___/|_|  \__,_|  \____/\___/ \__,_|_| |_|\__|
                                                      
                                                      
=======================================================================

-------------------------------------------
> Create a maven project
-------------------------------------------
-------------------------------------------
/hadoop-word-count/src/main/java/ro/bbs/bogdan/WordCount
-------------------------------------------
=======================================================================
package ro.bbs.bogdan;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.StringTokenizer;

public class WordCount extends Configured implements Tool {

    public static class Map
            extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                word.set(tokenizer.nextToken());
                context.write(word, one);
            }
        }
    }

    public static class Reduce
            extends Reducer<Text, IntWritable, Text, IntWritable> {
        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {

            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            context.write(key, new IntWritable(sum));
        }
    }

    public int run(String[] args) throws Exception {
        Job job = new Job(getConf());
        job.setJarByClass(WordCount.class);
        job.setJobName("wordcount");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(Map.class);
        job.setCombinerClass(Reduce.class);
        job.setReducerClass(Reduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        String inputPath = args.length >=1 && args[0] != null ? args[0] : "/input";
        String outputPath = args.length >=2 && args[1] != null ? args[1] : "/output";

        FileInputFormat.setInputPaths(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));

        boolean success = job.waitForCompletion(true);
        return success ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int ret = ToolRunner.run(new WordCount(), args);
        System.exit(ret);
    }
}

=======================================================================


-------------------------------------------
hadoop-word-count/pom.xml
-------------------------------------------

=======================================================================

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ro.bbs.bogdan</groupId>
    <artifactId>hadoop-word-count</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-core</artifactId>
            <version>1.2.1</version>
        </dependency>
    </dependencies>
</project>

=======================================================================



-------------------------------------------
> Build the project
-------------------------------------------
cd /home/bogdan/Desktop/Playground/grid-cluster-cloud-computing/hadoop/hadoop-word-count
mvn clean install


-------------------------------------------
> Create basic file system from nn1
-------------------------------------------
(by default under / we don't have anything =>
we have to create /user/root because one a simple
hadoop fs -ls the default path is /user/[currentSystemUser]
in our case root)

hadoop fs -mkdir /user
hadoop fs -mkdir /user/root
hadoop fs -chmod 777 /user/root


-------------------------------------------
> Download arabian nights to your local machine (outside vm)
-------------------------------------------
cd ~/Desktop/
wget https://www.gutenberg.org/files/34206/34206-0.txt
mv 34206-0.txt the-thousand-and-one-nights-vol-I



-------------------------------------------
> SCP Copy arabian nights from local to nn1
-------------------------------------------
scp /home/bogdan/Desktop/the-thousand-and-one-nights-vol-I root@192.168.43.73:/home/root/



-------------------------------------------
> Syntax to copy file(s) from nn1 to HDFS
hadoop fs -put <local-src> <HDFS_dest_path>
-------------------------------------------
cd ~/../../home/root/
hadoop fs -put ./the-thousand-and-one-nights-vol-I /user/root/wordcount



-------------------------------------------
> SCP Copy hadoop-word-count-1.0-SNAPSHOT.jar from local to nn1
-------------------------------------------
scp /home/bogdan/Desktop/Playground/grid-cluster-cloud-computing/hadoop/hadoop-word-count/target/hadoop-word-count-1.0-SNAPSHOT.jar  root@192.168.43.73:/home/root/


-------------------------------------------
> Run word count job from nn1
-------------------------------------------
cd ~/../../home/root/
fs -rm -r -f /user/root/wordcount/output
hadoop jar hadoop-word-count-1.0-SNAPSHOT.jar ro.bbs.bogdan.WordCount '/user/root/wordcount/the-thousand-and-one-nights-vol-I' '/user/root/wordcount/output'

...success :) ...

hadoop fs -ls /user/root/wordcount/output
hadoop fs -cat /user/root/wordcount/output/part-r-00000











=======================================================================

 _____                    _           _   _____          _           
|_   _|                  | |         | | |_   _|        | |          
  | | _ ____   _____ _ __| |_ ___  __| |   | | _ __   __| | _____  __
  | || '_ \ \ / / _ \ '__| __/ _ \/ _` |   | || '_ \ / _` |/ _ \ \/ /
 _| || | | \ V /  __/ |  | ||  __/ (_| |  _| || | | | (_| |  __/>  < 
 \___/_| |_|\_/ \___|_|   \__\___|\__,_|  \___/_| |_|\__,_|\___/_/\_\
                                                                                                                                         
                                                      
=======================================================================


-------------------------------------------
> Create a maven project
-------------------------------------------
-------------------------------------------
/hadoop-word-count/src/main/java/ro/bbs/bogdan/InvertedIndex
-------------------------------------------
=======================================================================
package ro.bbs.bogdan;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class InvertedIndex
{
    private static List<String> stopwords = new ArrayList<String>();

    public static class TokenizerMapper extends Mapper<Object, Text, Text, Text>
    {
        // the current word holder
        private Text word = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            // we get the name of the file
            FileSplit fileSplit = (FileSplit)context.getInputSplit();
            Text filename = new Text(fileSplit.getPath().getName());

            // the iterator
            StringTokenizer itr = new StringTokenizer(value.toString());
            // while we have more words
            while (itr.hasMoreTokens())
            {
                // we get the current word
                word.set(itr.nextToken());
                // if it's not a stopword
                if(!stopwords.contains(word.toString().trim()))
                {
                    // we write the word and the belonging file
                    context.write(word, filename);
                }
            }
        }
    }

    public static class FilenameReducer extends Reducer<Text,Text,Text,Text>
    {
        private Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            // we remove the duplicates
            Set<Text> filenamesSet = new LinkedHashSet<Text>();

            for (Text filename : values)
            {
                filenamesSet.add(filename);
            }

            StringBuilder filenamesListString = new StringBuilder();
            filenamesListString.append("|");

            for (Text filename : filenamesSet)
            {
                filenamesListString.append(" ").append(filename.toString()).append( " |");
            }

            // we set the list string as a result
            result.set(filenamesListString.toString());
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "inverted index");
        job.setJarByClass(InvertedIndex.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(FilenameReducer.class);
        job.setReducerClass(FilenameReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        String stopwordsPath = args.length >=2 && args[2] != null ? args[2] : "/user/root/invertedindex/stopwords";
        try{
            Path pt=new Path(stopwordsPath);//Location of file in HDFS
            FileSystem fs = FileSystem.get(conf);
            BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(pt)));
            String line;
            line=br.readLine();
            while (line != null){
                stopwords.add(line.trim());
                line=br.readLine();
            }
        }
        catch(Exception e)
        {
            System.out.println("Invalid stopwords file :( ");
        }
        String inputPath = args.length >=1 && args[0] != null ? args[0] : "/user/root/invertedindex/inputbooks";
        String outputPath = args.length >=2 && args[1] != null ? args[1] : "/user/root/invertedindex/output";
        FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));


        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

=======================================================================


-------------------------------------------
hadoop-word-count/pom.xml
-------------------------------------------

=======================================================================

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ro.bbs.bogdan</groupId>
    <artifactId>hadoop-inverted-index</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-core</artifactId>
            <version>1.2.1</version>
        </dependency>
    </dependencies>
</project>

=======================================================================



-------------------------------------------
> Build the project
-------------------------------------------
cd /home/bogdan/Desktop/Playground/grid-cluster-cloud-computing/hadoop/hadoop-inverted-index
mvn clean install


-------------------------------------------
> Create basic file system from nn1
-------------------------------------------
(by default under / we don't have anything =>
we have to create /user/root because one a simple
hadoop fs -ls the default path is /user/[currentSystemUser]
in our case root)

hadoop fs -mkdir /user
hadoop fs -mkdir /user/root
hadoop fs -chmod 777 /user/root


-------------------------------------------
> Download arabian nights + other books to nn1 
-------------------------------------------
cd  /home/root
wget https://www.gutenberg.org/files/34206/34206-0.txt
mv 34206-0.txt the-thousand-and-one-nights-vol-I
...


-------------------------------------------
> Syntax to copy file(s) from nn1 to HDFS
hadoop fs -put <local-src> <HDFS_dest_path>
-------------------------------------------
cd ~/../../home/root/
hadoop fs -put ./the-thousand-and-one-nights-vol-I /user/root/invertedindex/inputbooks


-------------------------------------------
> Create the stopwords file on nn1
-------------------------------------------
cd ~/../../home/root/
vi stopwords
...
Hello
Goodbye
Here
come
the
words
we 
exclude
...

-------------------------------------------
> Syntax to copy file(s) from nn1 to HDFS
hadoop fs -put <local-src> <HDFS_dest_path>
-------------------------------------------
cd ~/../../home/root/
hadoop fs -put ./stopwords /user/root/invertedindex


-------------------------------------------
> SCP Copy hadoop-word-count-1.0-SNAPSHOT.jar from local to nn1
-------------------------------------------
scp /home/bogdan/Desktop/Playground/grid-cluster-cloud-computing/hadoop/hadoop-inverted-index/target/hadoop-inverted-index-1.0-SNAPSHOT.jar  root@192.168.43.73:/home/root/


-------------------------------------------
> Run word count job from nn1
-------------------------------------------
cd ~/../../home/root/
hadoop fs -rm -r -f /user/root/invertedindex/output
hadoop jar hadoop-inverted-index-1.0-SNAPSHOT.jar ro.bbs.bogdan.InvertedIndex '/user/root/invertedindex/inputbooks' '/user/root/invertedindex/output' '/user/root/invertedindex/stopwords'

...success :) ...

hadoop fs -ls /user/root/invertedindex/output
hadoop fs -cat /user/root/invertedindex/output/part-r-00000

